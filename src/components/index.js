import React, {Component} from 'react'
import ReactDom from 'react-dom'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import '../css/index.css'
import Typography from '@material-ui/core/Typography'

import {HashRouter, Switch, Route} from 'react-router-dom'


import {Link} from 'react-router-dom'

class LandingPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: "Testing shit"
    }

  }

  render() {
    return (
      <Grid container>
        <Grid sm={6} className='centering-left'>
          <div>
            <Typography variant="display1" gutterBottom>
              Welcome to Swift Clean
            </Typography>

            <Typography variant="caption" gutterBottom align="center">
              Made Of Speed
            </Typography>
          </div>
        </Grid>

        <Grid xs={12} sm={6} className='centering'>
          <Button variant='raised' color='primary'>
            Boom!
          </Button>
          <Typography variant="display1" gutterBottom align="center">
            <Link to='/next'>Next</Link>
          </Typography>
        </Grid>
      </Grid>
    );
  }

}




class AnotherComponent extends Component {
  constructor(props) {
    super(props)
    this.handleBackClick = this.handleBackClick.bind(this)
  }

  handleBackClick () {
    const {history} = this.props
    history.push('/')
  }

  render() {
    return (
      <Grid sm={6} align="center">
        <Typography gutterBottom>
          This is the new order. The is material ui
        </Typography>

        <Button variant='flat' color='secondary' onClick={this.handleBackClick}>
          Back
        </Button>
      </Grid>
    )
  }
}


class App extends Component {

  render () {
    return (
      <main>
        <Switch>
          <Route exact path='/' component={LandingPage}/>
          <Route path='/next' component={AnotherComponent}/>
        </Switch>
      </main>
    )
  }
}


ReactDom.render(
  <HashRouter>
    <App />
  </HashRouter>,
  document.getElementById('mainNode')
)
